#!/usr/bin/env ruby

$: << File.join(File.dirname(__FILE__), 'lib')

require 'letty'
require 'spinny/pure_io'

class Hello
  fn main =
    putStrLn("What is your name?") >>
      readLine(:name) >>
      putStrLn "Hi there, {name}! How are you?" >>
      readLine(:resp) >>
      putStrLn "I am also {resp}!" >>
      exit(0)
end
