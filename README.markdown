# Spinny::PureIO

Pure IO for Ruby.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'spinny-pure_io'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install spinny-pure_io

## Usage

```ruby
require 'letty'
require 'spinny/pure_io'

class Hello
  fn main =
    putStrLn("What is your name?") >>
      readLine(:name) >>
      putStrLn "Hi there, {name}! How are you?" >>
      readLine(:resp) >>
      putStrLn "I am also {resp}!" >>
      exit(0)
end
```

## Contributing

1. Fork it ( https://github.com/[my-github-username]/spinny-pure_io/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
