require 'spinny/pure_io/version'

module Spinny::PureIO::Unpure
  IO = ::IO
  Object.send(:remove_const, :IO)

  %w[
      print printf putc puts p
      gets readline
      exit exit!
  ].each do |fn|
    define_singleton_method(fn, &Kernel.method(fn))

    Kernel.class_exec do
      eval("undef #{fn}")
    end
  end
end
